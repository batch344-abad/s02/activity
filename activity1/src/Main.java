import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year");
        int year = myScanner.nextInt();

        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)){
            System.out.println(year + " is a leap year");
        }
        else{
            System.out.println(year + " NOT is a leap year");
        }

    }
}



